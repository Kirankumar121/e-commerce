package com.example.ecom.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.example.ecom.model.Cart;
import com.example.ecom.model.Product;
import com.example.ecom.model.User;
import com.example.ecom.repository.CartRepository;
import com.example.ecom.repository.ProductRepository;
import com.example.ecom.repository.UserRepository;

@Service
public class EcomService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	CartRepository cartRepository;
	
	@Autowired
    MongoTemplate mongoTemplate;
	
	public void saveUser(User user) {
        userRepository.save(user);
    }

	public User findUserByEmail(String email) {
		User user = null;
		if(email != null && !email.isEmpty()){
			user = userRepository.findByEmail(email);
		}
		return user;
	}

	public List<Product> getAllProduct() {
		List<Product> list = productRepository.findAll();
		return list;
	}

	public Product getProductById(String pid) {
		Optional<Product> product = productRepository.findById(pid);
		return product.get();
	}

	public Cart getCart(String pid, String uid) {
		 Query query = new Query();
	     query.addCriteria(Criteria.where("pid").is(pid));
	     query.addCriteria(Criteria.where("uid").is(uid));
	     return mongoTemplate.findOne(query, Cart.class);
	}

	public void saveCart(Cart cart) {
		cartRepository.save(cart);
	}

	public List<Cart> getAllCart(String uid) {
		Query query = new Query();
	    query.addCriteria(Criteria.where("uid").is(uid));
		return mongoTemplate.find(query, Cart.class);
	}

	public void removeFromCart(String cid) {
		cartRepository.deleteById(cid);
	}

	public void removeCart(String uid) {
		Query query = new Query();
	    query.addCriteria(Criteria.where("uid").is(uid));
		mongoTemplate.remove(query, Cart.class);
	}
	
}
