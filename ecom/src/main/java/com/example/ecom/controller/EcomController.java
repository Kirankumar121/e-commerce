package com.example.ecom.controller;

import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.ecom.model.Cart;
import com.example.ecom.model.Product;
import com.example.ecom.model.User;
import com.example.ecom.service.EcomService;

@RestController
public class EcomController {
	@Autowired
	EcomService ecomService; 
	
	@RequestMapping(value = {"/","/home"}, method = RequestMethod.GET)
	public ModelAndView home() {
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("home");
	    return modelAndView;
	}
	
	@RequestMapping(value = {"/signup"}, method = RequestMethod.GET)
	public ModelAndView signup() {
	    ModelAndView modelAndView = new ModelAndView();
	    User user = new User();
        modelAndView.addObject("user", user);
	    modelAndView.setViewName("signup");
	    return modelAndView;
	}
	
	@RequestMapping(value = {"/login"}, method = RequestMethod.GET)
	public ModelAndView login() {
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("login");
	    return modelAndView;
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public ModelAndView createNewUser(User user) {
	    ModelAndView modelAndView = new ModelAndView();
	    User userExists = ecomService.findUserByEmail(user.getEmail());
	    if (userExists != null) {
	    	modelAndView.addObject("successMessage", "There is already a user registered with the username provided");
	    }else {
	    	ecomService.saveUser(user);
	        modelAndView.addObject("successMessage", "User has been registered successfully");
	        modelAndView.addObject("user", new User());
	        modelAndView.setViewName("login");

	    }
	    return modelAndView;
	}
	
	@RequestMapping(value = {"/login"}, method = RequestMethod.POST)
	public ModelAndView checkLogin(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView();
		String email = request.getParameter("email");
		String pass = request.getParameter("password");
		User userExists = ecomService.findUserByEmail(email);
		if (userExists != null && pass.equals(userExists.getPassword())) {
			HttpSession session = request.getSession();
			session.setAttribute("login", userExists.getId());
			List<Product> list = ecomService.getAllProduct();
			modelAndView.addObject("paginationProducts", list);
			modelAndView.setViewName("dashboard");
		}else {
			modelAndView.addObject("successMessage", "Wrong Username OR Password");
			modelAndView.setViewName("login");
		}
	    return modelAndView;
	}
	
	
	@RequestMapping(value = {"/addToCart"}, method = RequestMethod.GET)
	public ModelAndView addToCart(HttpServletRequest request) {
		String pid = request.getParameter("id");
		HttpSession session = request.getSession(false);
		String uid = session.getAttribute("login").toString();
		Product product = ecomService.getProductById(pid);
		Cart old_cart  = ecomService.getCart(pid, uid);
		
		if (old_cart != null) {
			old_cart.setQuantity(old_cart.getQuantity()+1);
			old_cart.setPrice(product.getPrice()+old_cart.getPrice());
			ecomService.saveCart(old_cart);
		}else {
			Cart cart  = new Cart();
			cart.setPname(product.getId());
			cart.setPid(pid);
			cart.setUid(uid);
			cart.setQuantity(1);
			cart.setPrice(product.getPrice());
			cart.setPname(product.getName());
			ecomService.saveCart(cart);
		}
		
		ModelAndView modelAndView = new ModelAndView();
		List<Cart> cart_list = ecomService.getAllCart(uid);
		modelAndView.addObject("cart", cart_list);
	    List<Product> list = ecomService.getAllProduct();
		modelAndView.addObject("paginationProducts", list);
		modelAndView.setViewName("dashboard");
	    return modelAndView;
	}
	
	@RequestMapping(value = {"/removeFromCart"}, method = RequestMethod.GET)
	public ModelAndView removeFromCart(HttpServletRequest request) {
		String cid = request.getParameter("id");
		HttpSession session = request.getSession(false);
		String uid = session.getAttribute("login").toString();
		
		ecomService.removeFromCart(cid);
		
		ModelAndView modelAndView = new ModelAndView();
		List<Cart> cart_list = ecomService.getAllCart(uid);
		modelAndView.addObject("cart", cart_list);
	    List<Product> list = ecomService.getAllProduct();
		modelAndView.addObject("paginationProducts", list);
		modelAndView.setViewName("dashboard");
	    return modelAndView;
	}
	
	@RequestMapping(value = {"/checkout"}, method = RequestMethod.GET)
	public ModelAndView checkout(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		String uid = session.getAttribute("login").toString();
		List<Cart> cart_list = ecomService.getAllCart(uid);
		int total = 0;
		for (int i = 0; i < cart_list.size(); i++) {
			total = total+cart_list.get(i).getPrice();
		}
		ecomService.removeCart(uid);
		Random random = new Random();
		String oid = String.format("%04d", random.nextInt(10000));
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.addObject("oid", oid);
	    modelAndView.addObject("total", total);
	    modelAndView.setViewName("checkout");
	    return modelAndView;
	}
	
	@RequestMapping(value = {"/products"}, method = RequestMethod.GET)
	public ModelAndView products() {
	    ModelAndView modelAndView = new ModelAndView();
	    List<Product> list = ecomService.getAllProduct();
		modelAndView.addObject("paginationProducts", list);
		modelAndView.setViewName("dashboard");
	    return modelAndView;
	}

}
