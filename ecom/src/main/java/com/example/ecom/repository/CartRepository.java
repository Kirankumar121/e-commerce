package com.example.ecom.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.ecom.model.Cart;

public interface CartRepository extends MongoRepository<Cart, String>{

}
