package com.example.ecom.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.ecom.model.Product;

public interface ProductRepository extends MongoRepository<Product,String>{
	
}
