package com.example.ecom.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.ecom.model.User;

public interface UserRepository extends MongoRepository<User,Long>{
	User findByEmail(String email);

}
